﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockManagementSystemWebApp.BLL.Models
{
    public class SearchByDate
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
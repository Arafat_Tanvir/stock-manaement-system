﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockManagementSystemWebApp.BLL.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Reorder { get; set; }
        public int CategoryId { get; set; }
        public int CompanyId { get; set; }
        public DateTime Date { get; set; }
    }
}
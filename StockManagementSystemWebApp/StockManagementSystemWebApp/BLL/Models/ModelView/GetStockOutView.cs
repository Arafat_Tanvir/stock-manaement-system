﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockManagementSystemWebApp.BLL.Models.ModelView
{
    public class GetStockOutView
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public DateTime Date { get; set; }
        public int StockOutQuantity { get; set; }
        public int Status { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockManagementSystemWebApp.BLL.Models
{
    public class GetStockInView
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int Reorder { get; set; }
        public int StockInQuantity { get; set; }
        public string Date { get; set; }
        
    }
}
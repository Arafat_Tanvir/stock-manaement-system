﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.DAL;

namespace StockManagementSystemWebApp.BLL.Manager
{
    public class CompanyManager
    {
        private CompanyGateway companyGateway;
        public CompanyManager()
        {
            companyGateway=new CompanyGateway();
        }
        
      
        public string Save(Company company)
        {
            bool IsCategoryNameExists = companyGateway.IsCompanyNameExists(company.Name);
            if (IsCategoryNameExists)
            {
                return "This Company Name All Ready Exists";
            }

            int rowAffect = companyGateway.Save(company);
            if (rowAffect > 0)
            {
                return "Save Successfully";
            }
            return "Error! Some Error Occurs";
   
        }

        //Show ALL Companies
        public List<Company> ShowAllCompanies()
        {
            return companyGateway.ShowAllCompanies();

        }
        public Company GetCompanyById(int id)
        {
            return companyGateway.GetCompanyById(id);
        }

        public string UpdateCategory(Company company)
        {
            int rowAffect = companyGateway.UpdateCompany(company);
            if (rowAffect > 0)
            {
                return "Update Successfully";
            }
            return "Error! Some Error Occurs";
        }
    }
}
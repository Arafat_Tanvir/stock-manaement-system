﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.DAL;

namespace StockManagementSystemWebApp.BLL.Manager
{
    public class CategoryManager
    {
        private CategoryGateway categoryGateway;
        public CategoryManager()
        {
            categoryGateway=new CategoryGateway();
        }
        
      
        public string Save(Category category)
        {
            bool IsCategoryNameExists = categoryGateway.IsCategoryNameExists(category.Name);
            if (IsCategoryNameExists)
            {
                return "This Category Name All Ready Exists";
            }

            int rowAffect = categoryGateway.Save(category);
            if (rowAffect > 0)
            {
                return "Save Successfully";
            }
            return "Error! Some Error Occurs";
   
        }

        //Show All Categories
        public List<Category> ShowAllCategories()
        {
            return categoryGateway.ShowAllCategories();

        }
        public Category GetCategoryById(int id)
        {
            return categoryGateway.GetCategoryById(id);
        }

        public string UpdateCategory(Category category)
        {
            int rowAffect = categoryGateway.UpdateCategory(category);
            if (rowAffect > 0)
            {
                return "Update Successfully";
            }
            return "Error! Some Error Occurs";
        }
    }
}
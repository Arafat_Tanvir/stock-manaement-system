﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.DAL;

namespace StockManagementSystemWebApp.BLL.Manager
{
    public class ItemManager
    {
         private ItemGateway itemGateway;
        public ItemManager()
        {
            itemGateway=new ItemGateway();
        }

        //Save Item 
        public string Save(Item item)
        {
            bool IsItemNameExists = itemGateway.IsItemNameExists(item.Name);
            if (IsItemNameExists)
            {
                return "This Item Name All Ready Exists";
            }
            else
            {
                int rowAffect = itemGateway.Save(item);
                if (rowAffect > 0)
                {
                    return "Save Successfully";
                }
                return "Error! Some Error Occurs";
            }
        }
        

       
        //load 
        public List<GetItemView> GetAllItemName(int id)
        {
            return itemGateway.GetAllItemName(id);
        }

        //Selected Item (id) Details
        public GetItemView GetItemDetailsViewById(int id)
        {
            return itemGateway.GetItemDetailsViewById(id);
        }

        //Get All Item By Using View(Includeing categoryName and companyName)
        public List<GetItemView> GetItemViewDetails()
        {
            return itemGateway.GetItemViewDetails();
        }

    }
}
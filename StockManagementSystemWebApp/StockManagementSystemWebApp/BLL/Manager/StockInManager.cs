﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.DAL;

namespace StockManagementSystemWebApp.BLL.Manager
{
    public class StockInManager
    {
        
        private StockInGateway stockInGateway;
        public StockInManager()
        {
            stockInGateway= new StockInGateway();
        }

        //Stockin data save
        public string Save(StockIn stockIn)
        {
            int rowAffect = stockInGateway.Save(stockIn);
            if (rowAffect > 0)
            {
                return "Save Successfully";
            }
            return "Error! Some Error Occurs";
        }



        //How Many Quantity Available In this Item
        public GetStockInView GetAvailabeQuatityViewById(int id)
        {
            return stockInGateway.GetAvailabeQuatityViewById(id);
        }




        //show ALL Item Details Using VIEW
        public List<GetStockInView> GetStockInView()
        {
            return stockInGateway.GetStockInView();
        }



        //When i stroe Same Item Quantity then it just Increase Item Quantity
        public string QuanityUpdateByItemId(StockIn stockIn)
        {
            int rowAffect = stockInGateway.QuanityUpdateByItemId(stockIn);
            if (rowAffect > 0)
            {
                return "Save Successfully";
            }
            return "Error! Some Error Occurs";
        }

        //Search All item Details By  Category Or Company 
        public List<GetStockInView> SearchByCategoryOrCompany(GetStockInView stockInView)
        {
            return stockInGateway.SearchByCategoryOrCompany(stockInView);
        }


        //stockInQuantity Update When Sell the Item
        public string QuanitySellByItemId(StockIn stockIn)
        {
            int rowAffect = stockInGateway.QuanitySellByItemId(stockIn);
            if (rowAffect > 0)
            {
                return "Quantity Sell Successfully";
            }
            return "Error! Some Error Occurs(Selling)";
        }

        public List<GetStockInView> GetAllItemNameViewById(int id)
        {
            return stockInGateway.GetAllItemNameViewById(id);
        }
    }
}
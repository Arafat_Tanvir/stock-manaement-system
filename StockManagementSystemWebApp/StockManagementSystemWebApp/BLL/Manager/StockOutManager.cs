﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.BLL.Models.ModelView;
using StockManagementSystemWebApp.DAL;

namespace StockManagementSystemWebApp.BLL.Manager
{
    public class StockOutManager
    {
        private StockOutGateway stockOutGateway;

        public StockOutManager()
        {
            stockOutGateway=new StockOutGateway();
        }

        public string Save(StockOut stockOut)
        {   
            int rowAffect = stockOutGateway.Save(stockOut);
            if (rowAffect > 0)
            {
                return "Save Successfully";
            }
            return "Error! Some Error Occurs";
            
        }

        public GetStockOutView StockOutQuatityView(int id)
        {
            return stockOutGateway.StockOutQuatityView(id);
        }

        public string QuanityUpdateByItemId(StockOut stockOut)  
        {
            int rowAffect = stockOutGateway.QuanityUpdateByItemId(stockOut);
            if (rowAffect > 0)
            {
                return "Save Successfully";
            }
            return "Error! Some Error Occurs";
        }
        //show all Importamti For StockOut
        public List<GetStockOutView> GetStockOutView()
        {
            return stockOutGateway.GetStockOutView();
        }
        //Update Status =1 When Item is Sell
        public string SellItem(StockOut stockOut)
        {
            int rowAffect = stockOutGateway.SellItem(stockOut);
            if (rowAffect > 0)
            {
                return " Sell Sucessfully";
            }
            return " Error! Some Error";
        }
        //search by data which is Sell
        public List<GetStockOutView> GetSearbyDate(SearchByDate searchByDate)
        {
            return stockOutGateway.GetSearbyDate(searchByDate);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.DAL;

namespace StockManagementSystemWebApp.UI.Sell
{
    public partial class SearchUI : System.Web.UI.Page
    {
        private StockOutManager stockOutManager;

        public SearchUI()
        {
            stockOutManager=new StockOutManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void searchByDate_OnClick(object sender, EventArgs e)
        {
            if (datePickerFrom.Text.Length == 0 || datePickerTo.Text.Length == 0)
            {
                messageLabel.Text = "Please Select Date ";
            }
            else
            {
                SearchByDate searchByDate=new SearchByDate();
                searchByDate.DateFrom = (Convert.ToDateTime(datePickerFrom.Text));
                searchByDate.DateTo = (Convert.ToDateTime(datePickerTo.Text));
                if (stockOutManager.GetSearbyDate(searchByDate).Count>0)
                {
                    searchByDateGridView.DataSource = stockOutManager.GetSearbyDate(searchByDate);
                    searchByDateGridView.DataBind();
                    datePickerFrom.Text = "";
                    datePickerTo.Text = "";
                    
                }
                else
                {

                    messageLabel.Text = "No Item Sell ";
                    datePickerFrom.Text = "";
                    datePickerTo.Text = "";
                   
                }
            
            }
        }

        protected void searchByDateGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
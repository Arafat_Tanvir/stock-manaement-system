﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;
using StockManagementSystemWebApp.BLL.Models;

namespace StockManagementSystemWebApp.UI.Sell
{
    public partial class SearchUI1 : System.Web.UI.Page
    {
        private CategoryManager categoryManager;
        private CompanyManager companyManager;
        private GetStockInView stockInView;
        private StockInManager stockInManager;
        public SearchUI1()
        {
            stockInManager=new StockInManager();
            stockInView=new GetStockInView();
            categoryManager=new CategoryManager();
            companyManager=new CompanyManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                categoryDropDownList.DataSource = categoryManager.ShowAllCategories();
                categoryDropDownList.DataBind();

                companyDropDownList.DataSource = companyManager.ShowAllCompanies();
                companyDropDownList.DataBind();
            }
        }

        protected void searchByCompanyOrCategory_OnClick(object sender, EventArgs e)
        {
            if (Convert.ToInt32(categoryDropDownList.SelectedValue) > 0 ||
                Convert.ToInt32(companyDropDownList.SelectedValue) > 0)
            {
                stockInView = new GetStockInView();
                stockInView.CategoryId = Convert.ToInt32(categoryDropDownList.SelectedItem.Value);
                stockInView.CompanyId = Convert.ToInt32(companyDropDownList.SelectedItem.Value);
                if (stockInManager.SearchByCategoryOrCompany(stockInView).Count > 0)
                {
                    showByCompanyOrCategory.DataSource = stockInManager.SearchByCategoryOrCompany(stockInView);
                    showByCompanyOrCategory.DataBind();
                  
                }
                else
                {
                    messageLabel.Text = "No Available Item";
                   
                }
                
               
            }
            else
            {
                messageLabel.Text = "Select Any One";
            }
            
        }

        protected void showByCompanyOrCategory_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }


       
    }
}
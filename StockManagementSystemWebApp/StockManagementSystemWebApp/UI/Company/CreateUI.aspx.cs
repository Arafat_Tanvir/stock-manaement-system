﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Company
{
    public partial class CreateUI : System.Web.UI.Page
    {
        private CompanyManager companyManager;

        public CreateUI()
        {
            companyManager=new CompanyManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        

        protected void saveButton_OnClick(object sender, EventArgs e)
        {
            BLL.Models.Company company = new BLL.Models.Company();
            company.Name = nameTextBox.Text;

            string message = companyManager.Save(company);
           // messageLabel.Text = message;
        }
    }
}
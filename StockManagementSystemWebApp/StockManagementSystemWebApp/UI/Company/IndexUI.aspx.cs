﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Company
{
    public partial class indexUI : System.Web.UI.Page
    {
         private CompanyManager companyManager;

        public indexUI()
        {
            companyManager=new CompanyManager();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            companyGridViewAll.DataSource = companyManager.ShowAllCompanies();
            companyGridViewAll.DataBind();
        }

       

        protected void updateLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = updateLinkButton.Parent as DataControlFieldCell;
            GridViewRow row = cell.Parent as GridViewRow;
            HiddenField idHiddenField = row.FindControl("companyIdHiddenField") as HiddenField;
            Response.Redirect("EditUI.aspx?Id=" + idHiddenField.Value);
        }


        protected void companyGridViewAll_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
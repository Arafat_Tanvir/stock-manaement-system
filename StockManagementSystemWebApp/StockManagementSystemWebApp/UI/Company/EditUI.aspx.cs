﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Company
{
    public partial class editUI : System.Web.UI.Page
    {
        private CompanyManager companyManager;

        public editUI()
        {
            companyManager=new CompanyManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                int id = Convert.ToInt32(Request.QueryString["Id"]);
                BLL.Models.Company company = companyManager.GetCompanyById(id);
                CompanyIdHiddenField.Value = company.Id.ToString();
                nameTextBox.Text = company.Name;
            }
        }

        protected void updateButton_OnClick(object sender, EventArgs e)
        {
            BLL.Models.Company company = new BLL.Models.Company();
            company.Id = Convert.ToInt32(CompanyIdHiddenField.Value);
            company.Name = nameTextBox.Text;
            companyManager.UpdateCategory(company);
            Response.Redirect("IndexUI.aspx");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Category
{
    public partial class CreateUI : System.Web.UI.Page
    {
         private CategoryManager categoryManager;

        public CreateUI()
        {
            categoryManager=new CategoryManager();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            BLL.Models.Category category = new BLL.Models.Category();
            category.Name = nameTextBox.Text;

            string message = categoryManager.Save(category);
            messageLabel.Text = message;
        }
    }
}
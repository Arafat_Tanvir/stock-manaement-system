﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Category
{
    public partial class editUI : System.Web.UI.Page
    {
        private CategoryManager categoryManager;

        public editUI()
        {
            categoryManager=new CategoryManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int id = Convert.ToInt32(Request.QueryString["Id"]);
                BLL.Models.Category category = categoryManager.GetCategoryById(id);
                categoryIdHiddenField.Value = category.Id.ToString();
                nameTextBox.Text = category.Name;
            }
            
          
        }

        protected void updateButton_Click(object sender, EventArgs e)
        {
            BLL.Models.Category category = new BLL.Models.Category();
            category.Id = Convert.ToInt32(categoryIdHiddenField.Value);
            category.Name = nameTextBox.Text;
            categoryManager.UpdateCategory(category);
            Response.Redirect("IndexUI.aspx");
        }
    }
}
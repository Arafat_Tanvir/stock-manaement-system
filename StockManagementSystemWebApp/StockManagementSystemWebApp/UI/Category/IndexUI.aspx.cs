﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;
using StockManagementSystemWebApp.BLL.Models;

namespace StockManagementSystemWebApp.UI.Categories
{
    public partial class IndexUI : System.Web.UI.Page
    {
        private CategoryManager categoryManager;

        public IndexUI()
        {
            categoryManager=new CategoryManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            categoryGridViewAll.DataSource = categoryManager.ShowAllCategories();
            categoryGridViewAll.DataBind();
        }

        protected void updateLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell=updateLinkButton.Parent as DataControlFieldCell;
            GridViewRow row=cell.Parent as GridViewRow;
            HiddenField idHiddenField=row.FindControl("categoryIdHiddenField") as HiddenField;
            Response.Redirect("EditUI.aspx?Id="+idHiddenField.Value);

        }

        protected void categoryGridViewAll_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                
                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
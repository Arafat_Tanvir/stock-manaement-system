﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Stock_In
{
    public partial class indexUI : System.Web.UI.Page
    {
        private StockInManager stockInManager;

        public indexUI()
        {
            stockInManager=new StockInManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            stockInGridViewAll.DataSource = stockInManager.GetStockInView();
            stockInGridViewAll.DataBind();
        }

        protected void stockInGridViewAll_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
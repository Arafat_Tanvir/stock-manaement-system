﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;
using StockManagementSystemWebApp.BLL.Models;

namespace StockManagementSystemWebApp.UI.Stock_In
{
    public partial class createUI : System.Web.UI.Page
    {
        private StockInManager stockInManager;
        private CompanyManager companyManager;
        private ItemManager itemManager;
        private BLL.Models.StockIn stockIn;
        public createUI()
        {
            stockInManager = new StockInManager();
            companyManager=new CompanyManager();
            itemManager=new ItemManager();  
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                companyDropDownList.DataSource = companyManager.ShowAllCompanies();
                companyDropDownList.DataBind();
            }
        }
       

        //when i select the company Id then show all item in dropdown list
        protected void loadAllItemName(object sender, EventArgs e)  
        {
            if (IsPostBack)
            {
                int id = Convert.ToInt32(companyDropDownList.SelectedItem.Value);

                if (itemManager.GetAllItemName(id).Count>0)
                {
                    if (itemDropDownList != null)
                    {
                        itemDropDownList.DataSource = itemManager.GetAllItemName(id);
                        itemDropDownList.DataBind();
                        itemDropDownList.Items.Insert(0, "===Select Item ===");
                    }
                    else
                    {
                        messageLabel.Text = "Select Item";
                    }
                }
                else
                {
                    messageLabel.Text = "No available Item";
                }
            }
            
            
        }

        protected void loadAllItemNameDetailsAndStockInQuantity(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(itemDropDownList.SelectedItem.Value);



            BLL.Models.GetItemView item = itemManager.GetItemDetailsViewById(id);
            reorderTextBox.Text = item.Reorder.ToString();


            if (stockInManager.GetAvailabeQuatityViewById(id) == null)
            {
                availableQuantityTextBox.Text = 0.ToString();
            }
            else
            {
                GetStockInView getStockInView = stockInManager.GetAvailabeQuatityViewById(id);
                availableQuantityTextBox.Text = getStockInView.StockInQuantity.ToString();
            }
           
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(itemDropDownList.SelectedItem.Value);

            if (stockInManager.GetAvailabeQuatityViewById(id) == null)
            {
                stockIn = new BLL.Models.StockIn();
                stockIn.ItemId = Convert.ToInt32(itemDropDownList.SelectedItem.Value);
                stockIn.StockInQuantity = Convert.ToInt32(stockInQuantityTextBox.Text);
                string message = stockInManager.Save(stockIn);
                messageLabel.Text = message;
            }
            else
            {
                int sum = 0;
                GetStockInView stockInView = stockInManager.GetAvailabeQuatityViewById(id);
                int totalStockInQuantity = stockInView.StockInQuantity;

                int newStockInQuantity = Convert.ToInt32(stockInQuantityTextBox.Text);


                sum = totalStockInQuantity + newStockInQuantity;

                stockIn = new StockIn();
                stockIn.Id = stockInView.Id;
                stockIn.StockInQuantity = sum;
                string message = stockInManager.QuanityUpdateByItemId(stockIn);
                messageLabel.Text = message;
            }
        }
    }
}
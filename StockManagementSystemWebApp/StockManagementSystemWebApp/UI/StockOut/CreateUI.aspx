﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateUI.aspx.cs" Inherits="StockManagementSystemWebApp.UI.Stock_Out.createUI" %>

<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Stock Management System</title>
      <link href="../../Public/Assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="../../Public/Assets/css/fonts.googleapis.css" rel="stylesheet" />
      <link href="../../Public/Assets/css/sb-admin-2.min.css" rel="stylesheet">
    <style>
        div.badge {
            color: red;
            font-weight: bold;
            font-style: italic;
        }
    </style>
</head>

<body id="page-top">
    <form id="categoryForm" runat="server">
      <div id="wrapper">
        <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">
            <asp:HyperLink CssClass="sidebar-brand d-flex align-items-center justify-content-center" NavigateUrl="../../HomeUI.aspx" runat="server">     
            <div class="sidebar-brand-text mx-3">
                <img src="../../Public/Assets/img/stock.png" class="text-center" height="60px" width="100px" />
            </div>
            </asp:HyperLink>
          <hr class="sidebar-divider my-0">
          <li class="nav-item active">
              <asp:HyperLink CssClass="nav-link" ID="HyperLink1" NavigateUrl="../../UI/DashboardUI.aspx" runat="server">
                  <i class="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span>
              </asp:HyperLink>
          </li>

          <hr class="sidebar-divider">

          <div class="sidebar-heading">
            STOCK MANAGEMENT SYSTEM
          </div>

           <%--for category link--%>
              <li class="nav-item">
                  <asp:HyperLink CssClass="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" ID="HyperLink2" runat="server">
                      <i class="fas fa-fw fa-cog"></i><span>Category</span>
                  </asp:HyperLink>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                      <div class="bg-white py-2 collapse-inner rounded">
                         <h6 class="collapse-header">Category Details</h6>
                          <asp:HyperLink CssClass="collapse-item" ID="HyperLink3" NavigateUrl="../../UI/Category/CreateUI.aspx" runat="server">
                              Create
                          </asp:HyperLink>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Category/IndexUI.aspx" ID="HyperLink4" runat="server">Show Category</asp:HyperLink>
                      </div>
                  </div>
              </li>

           <%--//for company Link--%>
           <li class="nav-item">
                <asp:HyperLink CssClass="nav-link collapsed" href="#" data-toggle="collapse" data-target="#company" aria-expanded="true" aria-controls="collapseTwo" ID="HyperLink5" runat="server">
                      <i class="fas fa-fw fa-cog"></i><span>Company</span>
                </asp:HyperLink>

                <div id="company" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Company Details</h6>
                      <asp:HyperLink CssClass="collapse-item" ID="HyperLink6" NavigateUrl="../../UI/Company/CreateUI.aspx" runat="server">
                          Create
                      </asp:HyperLink>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Company/IndexUI.aspx" ID="HyperLink7" runat="server">
                          Show Company
                      </asp:HyperLink>
                  </div>
                </div>
            </li>
          <hr class="sidebar-divider">
          <div class="sidebar-heading">
            Control Stock In And Stock Out
          </div>
            <%--//for item control--%>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#item" aria-expanded="true" aria-controls="collapseUtilities">
                  <i class="fas fa-fw fa-wrench"></i>
                  <span>Item</span>
                </a>

                <div id="item" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Item Details</h6>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Item/createUI.aspx" ID="HyperLink8" runat="server">
                          Create
                      </asp:HyperLink>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Item/indexUI.aspx" ID="HyperLink9" runat="server">
                          Item Details
                      </asp:HyperLink>
                  </div>
                </div>
            </li>
            
           <%-- for Stock in Control--%>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#stockIn" aria-expanded="true" aria-controls="collapsePages">
                  <i class="fas fa-fw fa-folder"></i>
                  <span>Stock In Details</span>
                </a>
                <div id="stockIn" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                      
                    <%--for stock in--%>
                    <h6 class="collapse-header">Stock In</h6>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockIn/CreateUI.aspx" ID="HyperLink12" runat="server">
                        Create
                    </asp:HyperLink>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockIn/IndexUI.aspx" ID="HyperLink13" runat="server">
                        StockIn Show
                    </asp:HyperLink>
                      
                    <div class="collapse-divider"></div>
                      <%--// for stock Out--%>
                    <h6 class="collapse-header">Stock Out:</h6>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockOut/CreateUI.aspx" ID="HyperLink14" runat="server">
                        Create
                    </asp:HyperLink>
                    
                  </div>
                </div>
            </li>
            
            <%--for search control--%>
              <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#search" aria-expanded="true" aria-controls="collapseUtilities">
                      <i class="fas fa-fw fa-wrench"></i>
                      <span>Search</span>
                    </a>
                    <div id="search" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                      <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Search</h6>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Sell/SearchByDateUI.aspx" ID="HyperLink10" runat="server">
                              Between Today Sell
                          </asp:HyperLink>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Sell/SearchUI.aspx" ID="HyperLink11" runat="server">
                             Item Search
                          </asp:HyperLink>
                      </div>
                    </div>  
             </li>

              

        </ul>
        <%--   end sidebar --%>
        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
              
             <%-- start navbar--%>
                <nav class="navbar navbar-expand navbar-light bg-light topbar mb-4 static-top shadow show">
                  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                  </button>
                  <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                       <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">Md.Kamrul Hasan</span>
                            </a>
                           <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Profile
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Logout
                            </a>
                          </div>
                      </li>
                  </ul>
                </nav>
              <%-- end navbar--%>
              

              <%-- Begin Page Content --%>
                <div class="container-fluid pb-5">
                    <div class="row justify-content-center">
                        <%--start 10 col-sm--%>
                        <div class="col-sm-2">
                            
                        </div>
                        <div class="col-sm-8">
                            <div class="text-center py-3">
                                <asp:Label ID="messageLabel" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">Stock Out Quantity</h1>
                                
                            </div>
                            <div class="card-body o-hidden card shadow-lg show py-5">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                         <asp:Label runat="server" Text="Select Company"></asp:Label>
                                    </div>
                                    <div class="col-sm-8">
                                         <div class="input-group">
                                            <asp:DropDownList ID="companyDropDownList" DataValueField="Id" DataTextField="Name" CssClass="form-control" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="loadAllItemNameSelected">
                                                <asp:ListItem Selected="True" Value="0">===Choose Company===</asp:ListItem>
                                            </asp:DropDownList>
                                          </div>
                                     </div>
                                  </div>
                              </div>
                            <div class="form-group">
                                 <div class="row">
                                    <div class="col-sm-4">
                                   <asp:Label ID="Label3" runat="server" Text="Select Item"></asp:Label>
                                        </div>
                                    <div class="col-sm-8">
                                  <div class="input-group">
                                      <asp:DropDownList ID="itemDropDownList" DataTextField="ItemName" DataValueField="ItemId" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="loadAllItemDetailsSelected">
                                          <asp:ListItem Selected="True" Value="0">===Choose Item===</asp:ListItem>
                                      </asp:DropDownList>
                                  </div>
                              </div>
                                    </div>
                              </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                <asp:Label  runat="server" Text="Reorder Level"></asp:Label>
                                        </div>
                                    <div class="col-sm-8">
                                <div class="input-group">
                                    <asp:TextBox ID="reorderTextBox" CssClass="form-control" runat="server"></asp:TextBox> <br />
                                </div>
                                 </div>
                                     </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                <asp:Label  runat="server" Text="Available Quantity"></asp:Label>
                                        </div>
                                    <div class="col-sm-8">
                                <div class="input-group">
                                    <asp:TextBox ID="availableQuantityTextBox" CssClass="form-control" runat="server"></asp:TextBox> <br />
                                </div>
                                 </div>
                                     </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                <asp:Label  runat="server" Text="Stock Out Quantity"></asp:Label>
                                        </div>
                                    <div class="col-sm-8">
                                <div class="input-group">
                                    <asp:TextBox ID="stockOutQuantityTextBox" CssClass="form-control" runat="server"></asp:TextBox> <br />
                                </div>
                            </div>

                                </div>
                                     </div>
                            <div class="form-group text-right">
                                <asp:Button ID="saveButton" CssClass="btn btn-success" runat="server" Text="Add" OnClick="saveButton_OnClick"  />
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            
                        </div>
                     </div>
                    <div class="row justify-content-center">
                        
                    <div class="col-sm-10">
                        <div class="text-center py-3">
                                <asp:Label ID="NoItemMessageLabel" runat="server" Text=""></asp:Label>
                            </div>
                        <asp:GridView ID="StockOutGridView" runat="server" CssClass="table" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                  <asp:TemplateField HeaderText="SL">
                                      <ItemTemplate>
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                           <asp:HiddenField id="itemIdHiddenField" runat="server" Value='<%#Eval("Id") %>'></asp:HiddenField>
                                      </ItemTemplate>
                                  </asp:TemplateField>
                                      
                                      <asp:TemplateField HeaderText="Company">
                                      <ItemTemplate>
                                          
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
                                         
                                      </ItemTemplate>
                                  </asp:TemplateField>
                                  
                                  <asp:TemplateField HeaderText="Item Name">
                                      <ItemTemplate>
                                          
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Eval("ItemName") %>'></asp:Label>
                                         
                                      </ItemTemplate>
                                  </asp:TemplateField>
                                  
                                  <asp:TemplateField HeaderText="Stock Out Quantity">
                                      <ItemTemplate>
                                          
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Eval("StockOutQuantity") %>'></asp:Label>
                                          
                                      </ItemTemplate>
                                  </asp:TemplateField>
                              </Columns>
                
                            <EditRowStyle BackColor="#7C6F57" />
                
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                
                        </asp:GridView>
                    </div>
                 </div>
                    <div class="row justify-content-center py-5 pb-5">
                    <div class="col-sm-4 text-center">
                         <asp:Button ID="sellButton" formnovalidate="formnovalidate" CssClass="btn btn-success" runat="server" Text="Sell" Width="229px" OnClick="sellButton_OnClick" />
                    </div>
                    <div class="col-sm-4 text-center">
                         <asp:Button ID="damageButton" formnovalidate="formnovalidate"  CssClass="btn btn-warning" runat="server" Text="Damage" Width="223px" OnClick="damageButton_OnClick" />
                    </div>
                    <div class="col-sm-4 text-center">
                         <asp:Button ID="lostButton" formnovalidate="formnovalidate"   CssClass="btn btn-danger" runat="server" Text="Lost" Width="233px" OnClick="lostButton_OnClick"  />
                    </div>
                </div>
                 </div>
              </div>

            <%--start footer--%>

          <footer class="sticky-footer card-footer bg-light shadow show py-5">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                <span>Copyright &copy; Your Website 2019</span>
                </div>
            </div>
         </footer>
     </div>
</div>
          <%-- Scroll to Top Button--%>
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>  
          <%--scripts for validation--%>
          <script src="../../Public/Assets/vendor/jquery/jquery.min.js"></script>
          <script src="../../Public/Assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
          <script src="../../Public/Assets/vendor/jquery-easing/jquery.easing.min.js"></script>
          <script src="../../Public/Assets/js/sb-admin-2.min.js"></script>

           <script src="../../Public/Assets/Validation/Scripts/jquery-3.3.1.js"></script>
           <script src="../../Public/Assets/Validation/Scripts/jquery.validate.js"></script>
           <script>
               $(document).ready(function () {
                   $.validator.setDefaults({
                       errorClass: "text-left badge badge-fill py-2",
                       highlight: function (element, errorClass) {
                           $(element).removeClass(errorClass);
                       },
                       errorPlacement: function (error, element) {
                           error.insertAfter($(element).parent('div'));
                       }
                   });
                   $.validator.addMethod('Item', function (value, element) {
                       return this.optional(element) || value > 0;
                   }, '===Select Item===');

                   $.validator.addMethod('company', function (value, element) {
                       return this.optional(element) || value > 0;
                   }, '===Select Company===');

                   $.validator.addMethod('Quantity', function (value, element) {
                       return this.optional(element) || value > 0 && /^\d*$/.test(value);
                   }, 'Input Must be Postive number');

                   $("#categoryForm").validate({
                       errorElement: 'div',
                       rules: {
                           itemDropDownList: {
                               required: true,
                               Item: true


                           },
                           companyDropDownList: {
                               required: true,
                               company: true
                           },

                           stockOutQuantityTextBox: {
                               required: true,
                               Quantity: true
                           }
                       },

                       messages: {
                           itemDropDownList: {
                               required: "===Select Item==="
                           },
                           companyDropDownList: {
                               required: "=Select Company==="
                           },

                           stockOutQuantityTextBox: {
                               required: "Enter Quanitity Value"
                           }
                       }
                   });
               });
           </script>
         
         <%-- end javascript--%>
    </form>
</body>

</html>

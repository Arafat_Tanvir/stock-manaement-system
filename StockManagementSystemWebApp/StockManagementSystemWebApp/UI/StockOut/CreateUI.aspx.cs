﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.BLL.Models.ModelView;
using StockManagementSystemWebApp.DAL;

namespace StockManagementSystemWebApp.UI.Stock_Out
{
    public partial class createUI : System.Web.UI.Page
    {
        private CompanyManager companyManager;
        private ItemManager itemManager;
        private StockInManager stockInManager;
        private StockOutManager stockOutManager;
        private BLL.Models.StockOut stockOut;
        private DateTime dateTimeNow;
       
        public createUI()
        {
            companyManager=new CompanyManager();
            itemManager=new ItemManager();
            stockInManager=new StockInManager();
            stockOutManager=new StockOutManager();
            dateTimeNow = DateTime.Now;
        }
        //load Company Name
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                companyDropDownList.DataSource = companyManager.ShowAllCompanies();
                companyDropDownList.DataBind();
            }
            if (stockOutManager.GetStockOutView().Count > 0)
            {
                StockOutGridView.DataSource = stockOutManager.GetStockOutView();
                StockOutGridView.DataBind();
            }
            else
            {
                NoItemMessageLabel.Text = "No Item In Card";
            }
            
           
            
        }

        
        //Load All item Name
        protected void loadAllItemNameSelected(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(companyDropDownList.SelectedItem.Value);
            if (stockInManager.GetAllItemNameViewById(id).Count > 0)
            {
                itemDropDownList.DataSource = stockInManager.GetAllItemNameViewById(id);
                itemDropDownList.DataBind();
                itemDropDownList.Items.Insert(0, "===Select Item ===");
            }
            else
            {
                messageLabel.Text = "No avaiable Item";
            }
            
        }

        protected void loadAllItemDetailsSelected(object sender, EventArgs e)
        {
            int sum = 0;
            int id = Convert.ToInt32(itemDropDownList.SelectedItem.Value);

            GetStockInView getStockInView=stockInManager.GetAvailabeQuatityViewById(id);

            reorderTextBox.Text = getStockInView.Reorder.ToString();

            int TotalStockInQuantity = getStockInView.StockInQuantity;


            if (stockOutManager.StockOutQuatityView(id) == null)
            {
                availableQuantityTextBox.Text = TotalStockInQuantity.ToString();
            }
            else
            {
                GetStockOutView getStockOutView = stockOutManager.StockOutQuatityView(id);
                int totalOutQuantity = getStockOutView.StockOutQuantity;

                if (TotalStockInQuantity > totalOutQuantity)
                {

                    sum = TotalStockInQuantity - totalOutQuantity;
                    availableQuantityTextBox.Text = sum.ToString();
                }
                else
                {
                    messageLabel.Text = "Not available Quantity";
                }
            }
        }
        protected void saveButton_OnClick(object sender, EventArgs e)
        {
            if (Convert.ToInt32(itemDropDownList.SelectedValue) > 0 &&
                Convert.ToInt32(companyDropDownList.SelectedValue) > 0 &&
                Convert.ToInt32(stockOutQuantityTextBox.Text) > 0)
            {
                int id = Convert.ToInt32(itemDropDownList.SelectedItem.Value);

                int sum = 0;

                GetStockInView getStockInView = stockInManager.GetAvailabeQuatityViewById(id);
                int TotalStockInQuantity = getStockInView.StockInQuantity;

                if (stockOutManager.StockOutQuatityView(id) == null)
                {
                    int newStockOutQuantity = Convert.ToInt32(stockOutQuantityTextBox.Text);
                    if (TotalStockInQuantity > newStockOutQuantity)
                    {
                        stockOut = new BLL.Models.StockOut();
                        stockOut.ItemId = Convert.ToInt32(itemDropDownList.SelectedItem.Value);
                        stockOut.Date = dateTimeNow.Date;
                        stockOut.StockOutQuantity = newStockOutQuantity;
                        stockOut.Status = 0;
                        string message = stockOutManager.Save(stockOut);
                        StockOutGridView.DataSource = stockOutManager.GetStockOutView();
                        StockOutGridView.DataBind();
                        messageLabel.Text = message;
                    }
                    else
                    {
                        messageLabel.Text = "Please Selecte less then " + TotalStockInQuantity + " Quantity";
                        Response.Redirect("CreateUI.aspx");
                    }
                    Response.Redirect("CreateUI.aspx");
                }
                else
                {
                    GetStockOutView getStockOutView = stockOutManager.StockOutQuatityView(id);
                    int totalOutQuantity = getStockOutView.StockOutQuantity;

                    int newstockOutQuanitity = Convert.ToInt32(stockOutQuantityTextBox.Text);
                    sum = totalOutQuantity + newstockOutQuanitity;

                    if (TotalStockInQuantity > sum)
                    {
                        stockOut = new BLL.Models.StockOut();
                        stockOut.Id = getStockOutView.Id;
                        stockOut.StockOutQuantity = sum;
                        string message = stockOutManager.QuanityUpdateByItemId(stockOut);
                        StockOutGridView.DataSource = stockOutManager.GetStockOutView();
                        StockOutGridView.DataBind();
                        messageLabel.Text = message;
                    }
                    else
                    {

                        messageLabel.Text = "NO Available Quantity";
                    }
                }
            }
            else
            {
                messageLabel.Text = "Please Select";
            }
        }

        protected void sellButton_OnClick(object sender, EventArgs e)
        {
            List<GetStockOutView> stockOutList = stockOutManager.GetStockOutView();            
            if (stockOutManager.GetStockOutView() == null)
            {
                Response.Write("NO available Item in Card");
            }
            else
            {
                foreach (GetStockOutView ItemStatusZeroStockOut in stockOutList)
                {
                    int sum = 0;

                    int id = ItemStatusZeroStockOut.ItemId;

                    //for stock in quantity
                    GetStockInView getStockInView = stockInManager.GetAvailabeQuatityViewById(id);
                    int TotalStockInQuantity = getStockInView.StockInQuantity;

                    //for stockout quantity
                    GetStockOutView getStockOutView = stockOutManager.StockOutQuatityView(id);
                    int totalOutQuantity = getStockOutView.StockOutQuantity;

                    if (TotalStockInQuantity > totalOutQuantity)
                    {
                        //for quantity update
                        sum = TotalStockInQuantity - totalOutQuantity;
                        StockIn stockIn = new StockIn();
                        stockIn.ItemId = ItemStatusZeroStockOut.ItemId;
                        stockIn.StockInQuantity = sum;
                        stockInManager.QuanitySellByItemId(stockIn);
                        
                        //for status
                        stockOut = new BLL.Models.StockOut();
                        stockOut.ItemId = ItemStatusZeroStockOut.ItemId;
                        stockOut.Status = 1;
                        stockOutManager.SellItem(stockOut);
                    }  
                }
                StockOutGridView.DataSource = stockOutManager.GetStockOutView();
                StockOutGridView.DataBind();
            }
        }

        protected void damageButton_OnClick(object sender, EventArgs e)
        {
            List<GetStockOutView> stockOutList = stockOutManager.GetStockOutView();
            if (stockOutManager.GetStockOutView() == null)
            {
                Response.Write("NO available Item in Card");
            }
            else
            {
                foreach (GetStockOutView ItemStatusZeroStockOut in stockOutList)
                {
                    int sum = 0;

                    int id = ItemStatusZeroStockOut.ItemId;

                    //for stock in quantity
                    GetStockInView getStockInView = stockInManager.GetAvailabeQuatityViewById(id);
                    int TotalStockInQuantity = getStockInView.StockInQuantity;

                    //for stockout quantity
                    GetStockOutView getStockOutView = stockOutManager.StockOutQuatityView(id);
                    int totalOutQuantity = getStockOutView.StockOutQuantity;

                    if (TotalStockInQuantity > totalOutQuantity)
                    {
                        //for quantity update
                        sum = TotalStockInQuantity - totalOutQuantity;
                        StockIn stockIn = new StockIn();
                        stockIn.ItemId = ItemStatusZeroStockOut.ItemId;
                        stockIn.StockInQuantity = sum;
                        stockInManager.QuanitySellByItemId(stockIn);

                        //for status
                        stockOut = new BLL.Models.StockOut();
                        stockOut.ItemId = ItemStatusZeroStockOut.ItemId;
                        stockOut.Status = 2;
                        stockOutManager.SellItem(stockOut);
                    }
                }
                StockOutGridView.DataSource = stockOutManager.GetStockOutView();
                StockOutGridView.DataBind();
            }
            
        }

        protected void lostButton_OnClick(object sender, EventArgs e)
        {
            List<GetStockOutView> stockOutList = stockOutManager.GetStockOutView();
            if (stockOutManager.GetStockOutView() == null)
            {
                Response.Write("NO available Item in Card");
            }
            else
            {
                foreach (GetStockOutView ItemStatusZeroStockOut in stockOutList)
                {
                    int sum = 0;

                    int id = ItemStatusZeroStockOut.ItemId;

                    //for stock in quantity
                    GetStockInView getStockInView = stockInManager.GetAvailabeQuatityViewById(id);
                    int TotalStockInQuantity = getStockInView.StockInQuantity;

                    //for stockout quantity
                    GetStockOutView getStockOutView = stockOutManager.StockOutQuatityView(id);
                    int totalOutQuantity = getStockOutView.StockOutQuantity;

                    if (TotalStockInQuantity > totalOutQuantity)
                    {
                        //for quantity update
                        sum = TotalStockInQuantity - totalOutQuantity;
                        StockIn stockIn = new StockIn();
                        stockIn.ItemId = ItemStatusZeroStockOut.ItemId;
                        stockIn.StockInQuantity = sum;
                        stockInManager.QuanitySellByItemId(stockIn);

                        //for status
                        stockOut = new BLL.Models.StockOut();
                        stockOut.ItemId = ItemStatusZeroStockOut.ItemId;
                        stockOut.Status = 3;
                        stockOutManager.SellItem(stockOut);
                    }
                }
                StockOutGridView.DataSource = stockOutManager.GetStockOutView();
                StockOutGridView.DataBind();
            }
        }
    }
}
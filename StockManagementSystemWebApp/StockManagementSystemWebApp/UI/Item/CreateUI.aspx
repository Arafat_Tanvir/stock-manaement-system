﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateUI.aspx.cs" Inherits="StockManagementSystemWebApp.UI.Item.createUI" %>

<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Stock Management System</title>
      <link href="../../Public/Assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="../../Public/Assets/css/fonts.googleapis.css" rel="stylesheet" />
      <link href="../../Public/Assets/css/sb-admin-2.min.css" rel="stylesheet">
    <style>
            div.badge {
                color: red;
                font-weight: bold;
                font-style: italic;
            }
        </style>
</head>

<body id="page-top">
    <form id="categoryForm" runat="server">
      <div id="wrapper">
        <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">
            <asp:HyperLink CssClass="sidebar-brand d-flex align-items-center justify-content-center" NavigateUrl="../../HomeUI.aspx" runat="server">     
            <div class="sidebar-brand-text mx-3">
                <img src="../../Public/Assets/img/stock.png" class="text-center" height="60px" width="100px" />
            </div>
            </asp:HyperLink>
          <hr class="sidebar-divider my-0">
          <li class="nav-item active">
              <asp:HyperLink CssClass="nav-link" ID="HyperLink1" NavigateUrl="../../UI/DashboardUI.aspx" runat="server">
                  <i class="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span>
              </asp:HyperLink>
          </li>

          <hr class="sidebar-divider">

          <div class="sidebar-heading">
            STOCK MANAGEMENT SYSTEM
          </div>

           <%--for category link--%>
              <li class="nav-item">
                  <asp:HyperLink CssClass="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" ID="HyperLink2" runat="server">
                      <i class="fas fa-fw fa-cog"></i><span>Category</span>
                  </asp:HyperLink>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                      <div class="bg-white py-2 collapse-inner rounded">
                         <h6 class="collapse-header">Category Details</h6>
                          <asp:HyperLink CssClass="collapse-item" ID="HyperLink3" NavigateUrl="../../UI/Category/CreateUI.aspx" runat="server">
                              Create
                          </asp:HyperLink>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Category/IndexUI.aspx" ID="HyperLink4" runat="server">Show Category</asp:HyperLink>
                      </div>
                  </div>
              </li>

           <%--//for company Link--%>
           <li class="nav-item">
                <asp:HyperLink CssClass="nav-link collapsed" href="#" data-toggle="collapse" data-target="#company" aria-expanded="true" aria-controls="collapseTwo" ID="HyperLink5" runat="server">
                      <i class="fas fa-fw fa-cog"></i><span>Company</span>
                </asp:HyperLink>

                <div id="company" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Company Details</h6>
                      <asp:HyperLink CssClass="collapse-item" ID="HyperLink6" NavigateUrl="../../UI/Company/CreateUI.aspx" runat="server">
                          Create
                      </asp:HyperLink>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Company/IndexUI.aspx" ID="HyperLink7" runat="server">
                          Show Company
                      </asp:HyperLink>
                  </div>
                </div>
            </li>
          <hr class="sidebar-divider">
          <div class="sidebar-heading">
            Control Stock In And Stock Out
          </div>
            <%--//for item control--%>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#item" aria-expanded="true" aria-controls="collapseUtilities">
                  <i class="fas fa-fw fa-wrench"></i>
                  <span>Item</span>
                </a>

                <div id="item" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Item Details</h6>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Item/createUI.aspx" ID="HyperLink8" runat="server">
                          Create
                      </asp:HyperLink>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Item/indexUI.aspx" ID="HyperLink9" runat="server">
                          Item Details
                      </asp:HyperLink>
                  </div>
                </div>
            </li>
            
           <%-- for Stock in Control--%>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#stockIn" aria-expanded="true" aria-controls="collapsePages">
                  <i class="fas fa-fw fa-folder"></i>
                  <span>Stock In Details</span>
                </a>
                <div id="stockIn" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                      
                    <%--for stock in--%>
                    <h6 class="collapse-header">Stock In</h6>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockIn/CreateUI.aspx" ID="HyperLink12" runat="server">
                        Create
                    </asp:HyperLink>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockIn/IndexUI.aspx" ID="HyperLink13" runat="server">
                        StockIn Show
                    </asp:HyperLink>
                      
                    <div class="collapse-divider"></div>
                      <%--// for stock Out--%>
                    <h6 class="collapse-header">Stock Out:</h6>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockOut/CreateUI.aspx" ID="HyperLink14" runat="server">
                        Create
                    </asp:HyperLink>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockOut/IndexUI.aspx" ID="HyperLink15" runat="server">
                        StockIn Show
                    </asp:HyperLink>
                  </div>
                </div>
            </li>
            
            <%--for search control--%>
              <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#search" aria-expanded="true" aria-controls="collapseUtilities">
                      <i class="fas fa-fw fa-wrench"></i>
                      <span>Search</span>
                    </a>
                    <div id="search" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                      <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Search</h6>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Sell/SearchByDateUI.aspx" ID="HyperLink10" runat="server">
                              Between Today Sell
                          </asp:HyperLink>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Sell/SearchUI.aspx" ID="HyperLink11" runat="server">
                             Item Search
                          </asp:HyperLink>
                      </div>
                    </div>  
             </li>

             
        </ul>
        <%--   end sidebar --%>
        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
              
             <%-- start navbar--%>
                <nav class="navbar navbar-expand navbar-light bg-light topbar mb-4 static-top shadow show">
                  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                  </button>
                  <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                       <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">Md.Kamrul Hasan</span>
                            </a>
                           <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Profile
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Logout
                            </a>
                          </div>
                      </li>
                  </ul>
                </nav>
              <%-- end navbar--%>
              

              <%-- Begin Page Content --%>
                <div class="container-fluid pb-5">
                    <div class="row">
                         <%--end start col-s--%>
                        <div class="col-sm-2">
                    
                        </div>
                        <%--end 1 col-s--%>
                        <%--start 10 col-sm--%>
                        <div class="col-sm-8">
                            <div class="text-center py-3">
                                <asp:Label ID="messageLabel" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">Category Create</h1>
                                <asp:HyperLink NavigateUrl="IndexUI.aspx" CssClass="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" runat="server">
                                    <i class="fas fa-plus-circle"></i>
                                    Back
                                </asp:HyperLink>
                            </div>
                            <div class="card-body o-hidden card shadow-lg show py-5">

                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-4">
                          <asp:Label ID="Label3" runat="server" Text="Category"></asp:Label>
                          </div>
                              <div class="col-sm-8">
                          <div class="input-group">
                              
                              <asp:DropDownList ID="categoryDropDownList" DataTextField = "Name" DataValueField="Id" CssClass="form-control" runat="server" AppendDataBoundItems="True" AutoPostBack="True"> 
                                  <asp:ListItem Selected="False" Value="0">===Choose Category===</asp:ListItem>
                                 
                              </asp:DropDownList>
                               </div>
                          </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-4">
                          <asp:Label ID="Label4" runat="server" Text="Company"></asp:Label>
                          </div>
                              <div class="col-sm-8">
                          <div class="input-group">
                              
                              <asp:DropDownList ID="companyDropDownList" DataTextField = "Name" DataValueField="Id" CssClass="form-control" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
                                  <asp:ListItem Selected="False" Value="0">===Chosse Company===</asp:ListItem>
                                 
                              </asp:DropDownList>
                               </div>
                          </div>
                          </div>
                      </div>
                                <div class="form-group">
                                  <div class="row">
                              <div class="col-sm-4">
                          <asp:Label ID="Label2" runat="server" Text="Item Name"></asp:Label>
                          </div>
                              <div class="col-sm-8">
                          <div class="input-group">
                               <asp:TextBox ID="nameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                          </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-4">
                          <asp:Label ID="Label5" runat="server" Text="Reorder Level"></asp:Label>
                          </div>
                              <div class="col-sm-8">
                          <div class="input-group">
                               <asp:TextBox ID="reorderTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                          </div>
                          </div>
                      </div>
                      <div class="form-group text-right">
                          <asp:Button ID="saveButton" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="saveButton_Click" />
                      </div>
                   </div>
                        </div>
                        
                         <%--end 10 col-s--%>
                        

                        <%-- start 1 col-sm--%>
                        <div class="col-sm-2">
                    
                        </div>
                        <%--end 1 col-s--%>
                      </div>
                   </div>
               </div>

            <%--start footer--%>

          <footer class="sticky-footer card-footer bg-light shadow show py-5">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                <span>Copyright &copy; Your Website 2019</span>
                </div>
            </div>
         </footer>
     </div>
</div>
          <%-- Scroll to Top Button--%>
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>  

          <%--  JavaScript--%>
        
           <script src="../../Public/Assets/Validation/Scripts/jquery-3.3.1.js"></script>
           <script src="../../Public/Assets/Validation/Scripts/jquery.validate.js"></script>
           <script>
               $(document).ready(function () {
                   $.validator.setDefaults({
                       errorClass: "text-left badge badge-fill py-2",
                       highlight: function (element, errorClass) {
                           $(element).removeClass(errorClass);
                       },
                       errorPlacement: function (error, element) {
                           error.insertAfter($(element).parent('div'));
                       }
                   });
                   $.validator.addMethod('category', function (value, element) {
                       return this.optional(element) || value > 0;
                   }, 'Select Category ');

                   $.validator.addMethod('company', function (value, element) {
                       return this.optional(element) || value > 0;
                   }, 'Select Company ');

                   $.validator.addMethod('nameTextBox', function (value, element) {
                       return this.optional(element) || value.length > 3 && /^[a-zA-Z\s]+$/i.test(value);
                   }, 'Name Length greater then 3');

                   $("#categoryForm").validate({
                       errorElement: 'div',
                       rules: {
                           categoryDropDownList: {
                               required: true,
                               category: true


                           },
                           companyDropDownList: {
                               required: true,
                               company: true
                           },

                           nameTextBox: {
                               required: true,
                               nameTextBox: true
                           }
                       },

                       messages: {
                           categoryDropDownList: {
                               required: "==Select Category=="
                           },
                           companyDropDownList: {
                               required: "==Select Company=="
                           },

                           nameTextBox: {
                               required: "Enter Your Name"
                           }
                       }
                   });
               });
           </script>
         
         <%-- end javascript--%>
    </form>
</body>

</html>

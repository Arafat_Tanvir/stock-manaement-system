﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IndexUI.aspx.cs" Inherits="StockManagementSystemWebApp.UI.Item.indexUI" %>

<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Stock Management System</title>
      <link href="../../Public/Assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="../../Public/Assets/css/fonts.googleapis.css" rel="stylesheet" />
      <link href="../../Public/Assets/css/sb-admin-2.min.css" rel="stylesheet">
      <link href="../../Public/datatable/css/bootstrap.css" rel="stylesheet" />
      <link href="../../Public/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
</head>

<body id="page-top">
    <form id="dashboard" runat="server">
      <div id="wrapper">
        <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">
            <asp:HyperLink CssClass="sidebar-brand d-flex align-items-center justify-content-center" NavigateUrl="../../HomeUI.aspx" runat="server">     
            <div class="sidebar-brand-text mx-3">
                <img src="../../Public/Assets/img/stock.png" class="text-center" height="60px" width="100px" />
            </div>
            </asp:HyperLink>
          <hr class="sidebar-divider my-0">
          <li class="nav-item active">
              <asp:HyperLink CssClass="nav-link" ID="HyperLink1" NavigateUrl="../../UI/DashboardUI.aspx" runat="server">
                  <i class="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span>
              </asp:HyperLink>
          </li>

          <hr class="sidebar-divider">

          <div class="sidebar-heading">
            STOCK MANAGEMENT SYSTEM
          </div>

           <%--for category link--%>
              <li class="nav-item">
                  <asp:HyperLink CssClass="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" ID="HyperLink2" runat="server">
                      <i class="fas fa-fw fa-cog"></i><span>Category</span>
                  </asp:HyperLink>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                      <div class="bg-white py-2 collapse-inner rounded">
                         <h6 class="collapse-header">Category Details</h6>
                          <asp:HyperLink CssClass="collapse-item" ID="HyperLink3" NavigateUrl="../../UI/Category/CreateUI.aspx" runat="server">
                              Create
                          </asp:HyperLink>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Category/IndexUI.aspx" ID="HyperLink4" runat="server">Show Category</asp:HyperLink>
                      </div>
                  </div>
              </li>

           <%--//for company Link--%>
           <li class="nav-item">
                <asp:HyperLink CssClass="nav-link collapsed" href="#" data-toggle="collapse" data-target="#company" aria-expanded="true" aria-controls="collapseTwo" ID="HyperLink5" runat="server">
                      <i class="fas fa-fw fa-cog"></i><span>Company</span>
                </asp:HyperLink>

                <div id="company" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Company Details</h6>
                      <asp:HyperLink CssClass="collapse-item" ID="HyperLink6" NavigateUrl="../../UI/Company/CreateUI.aspx" runat="server">
                          Create
                      </asp:HyperLink>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Company/IndexUI.aspx" ID="HyperLink7" runat="server">
                          Show Company
                      </asp:HyperLink>
                  </div>
                </div>
            </li>
          <hr class="sidebar-divider">
          <div class="sidebar-heading">
            Control Stock In And Stock Out
          </div>
            <%--//for item control--%>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#item" aria-expanded="true" aria-controls="collapseUtilities">
                 <i class="fab fa-pushed"></i>
                  <span>Item</span>
                </a>

                <div id="item" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Item Details</h6>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Item/createUI.aspx" ID="HyperLink8" runat="server">
                          Create
                      </asp:HyperLink>
                      <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Item/indexUI.aspx" ID="HyperLink9" runat="server">
                          Item Details
                      </asp:HyperLink>
                  </div>
                </div>
            </li>
            
           <%-- for Stock in Control--%>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#stockIn" aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-cogs"></i>
                  <span>Stock(In & Out)</span>
                </a>
                <div id="stockIn" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                      
                    <%--for stock in--%>
                    <h6 class="collapse-header">Stock In</h6>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockIn/CreateUI.aspx" ID="HyperLink12" runat="server">
                        Create
                    </asp:HyperLink>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockIn/IndexUI.aspx" ID="HyperLink13" runat="server">
                        StockIn Show
                    </asp:HyperLink>
                      
                    <div class="collapse-divider"></div>
                      <%--// for stock Out--%>
                    <h6 class="collapse-header">Stock Out:</h6>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockOut/CreateUI.aspx" ID="HyperLink14" runat="server">
                        Create
                    </asp:HyperLink>
                    <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/StockOut/IndexUI.aspx" ID="HyperLink15" runat="server">
                        StockIn Show
                    </asp:HyperLink>
                  </div>
                </div>
            </li>
            
            <%--for search control--%>
              <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#search" aria-expanded="true" aria-controls="collapseUtilities">
                     <i class="fas fa-search"></i>
                      <span>Search</span>
                    </a>
                    <div id="search" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                      <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Search</h6>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Sell/SearchByDateUI.aspx" ID="HyperLink10" runat="server">
                              Between Today Sell
                          </asp:HyperLink>
                          <asp:HyperLink CssClass="collapse-item" NavigateUrl="../../UI/Sell/SearchUI.aspx" ID="HyperLink11" runat="server">
                             Item Search
                          </asp:HyperLink>
                      </div>
                    </div>  
             </li>

        </ul>
        <%--   end sidebar --%>
        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
              
             <%-- start navbar--%>
                <nav class="navbar navbar-expand navbar-light bg-light topbar mb-4 static-top shadow show">
                 
                  <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                       <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">Md.Kamrul Hasan</span>
                            </a>
                           <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Profile
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Logout
                            </a>
                          </div>
                      </li>
                  </ul>
                </nav>
              <%-- end navbar--%>
              

              <%-- Begin Page Content --%>
                <div class="container-fluid pb-5">
                    <div class="row">
                         <%--end start col-s--%>
                        <div class="col-sm-1">
                    
                        </div>
                        <%--end 1 col-s--%>

                        <%--start col-sm--%>
                        <div class="col-sm-10">
                            <div class="text-center py-3">
                                <asp:Label ID="messageLabel" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">Item List</h1>
                                <asp:HyperLink NavigateUrl="CreateUI.aspx" CssClass="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" runat="server">
                                    <i class="fas fa-plus-circle"></i>
                                    Create
                                </asp:HyperLink>
                            </div>
                          <asp:GridView ID="itemGridViewAll" CssClass="table table-bordered" OnRowDataBound="itemGridViewAll_OnRowDataBound" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                             
                              <AlternatingRowStyle BackColor="White" />
                             
                              <Columns>
                                  <asp:TemplateField HeaderText="SL">
                                      <ItemTemplate>
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                           <asp:HiddenField id="itemIdHiddenField" runat="server" Value='<%#Eval("Id") %>'></asp:HiddenField>
                                      </ItemTemplate>
                                  </asp:TemplateField>
                              
                                  <asp:TemplateField HeaderText="Category">
                                      <ItemTemplate>
                                          
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Eval("CategoryName") %>'></asp:Label>
                                         
                                      </ItemTemplate>
                                  </asp:TemplateField>
                                      
                                      <asp:TemplateField HeaderText="Company">
                                      <ItemTemplate>
                                          
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
                                         
                                      </ItemTemplate>
                                  </asp:TemplateField>
                                  
                                  <asp:TemplateField HeaderText="Item Name">
                                      <ItemTemplate>
                                          
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                         
                                      </ItemTemplate>
                                  </asp:TemplateField>
                                  
                                  <asp:TemplateField HeaderText="Reorder Lavel">
                                      <ItemTemplate>
                                          
                                          <asp:Label CssClass="text-left" runat="server" Text='<%#Eval("Reorder") %>'></asp:Label>
                                          
                                      </ItemTemplate>
                                  </asp:TemplateField>
                             
                                  
                              </Columns>

                              <FooterStyle BackColor="#CCCC99" />
                              <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                              <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                              <RowStyle BackColor="#F7F7DE" />
                              <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                              <SortedAscendingCellStyle BackColor="#FBFBF2" />
                              <SortedAscendingHeaderStyle BackColor="#848384" />
                              <SortedDescendingCellStyle BackColor="#EAEAD3" />
                              <SortedDescendingHeaderStyle BackColor="#575357" />

                          </asp:GridView>
                         </div>
                        
                         <%--end 10 col-s--%>
                        

                        <%-- start 1 col-sm--%>
                        <div class="col-sm-1">
                    
                        </div>
                        <%--end 1 col-s--%>
                      </div>
                   </div>
               </div>

            <%--start footer--%>

          <footer class="sticky-footer card-footer bg-light shadow show py-5">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                <span>Copyright &copy; Your Website 2019</span>
                </div>
            </div>
         </footer>
     </div>
</div>
          <%-- Scroll to Top Button--%>
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>  

          <%--  JavaScript--%>
          <script src="../../Public/Assets/vendor/jquery/jquery.min.js"></script>
          <script src="../../Public/Assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
          <script src="../../Public/Assets/vendor/jquery-easing/jquery.easing.min.js"></script>
          <script src="../../Public/Assets/js/sb-admin-2.min.js"></script>


          <script src="../../Public/datatable/js/jquery-3.3.1.js"></script>
          <script src="../../Public/datatable/js/jquery.dataTables.min.js"></script>
          <script src="../../Public/datatable/js/dataTables.bootstrap4.min.js"></script>
          <script>
              $(document).ready(function () {
                  $('#<%= itemGridViewAll.ClientID %>').DataTable();
                });
          </script>
         <%-- end javascript--%>
    </form>
</body>

</html>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Item
{
    public partial class createUI : System.Web.UI.Page
    {
        private ItemManager itemManager;
        private CategoryManager categoryManager;
        private CompanyManager companyManager;

        public createUI()
        {
            categoryManager=new CategoryManager();
            companyManager=new CompanyManager();
            itemManager=new ItemManager();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                categoryDropDownList.DataSource =categoryManager.ShowAllCategories();
                categoryDropDownList.DataBind();

                companyDropDownList.DataSource = companyManager.ShowAllCompanies();
                companyDropDownList.DataBind();
            }
            
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            
            BLL.Models.Item item=new BLL.Models.Item();
            item.CategoryId = Convert.ToInt32(categoryDropDownList.SelectedItem.Value);
            item.CompanyId = Convert.ToInt32(companyDropDownList.SelectedItem.Value);
            item.Name = nameTextBox.Text;
            item.Reorder = Convert.ToInt32(reorderTextBox.Text);
            string message = itemManager.Save(item);
            messageLabel.Text = message;
        }
    }
}
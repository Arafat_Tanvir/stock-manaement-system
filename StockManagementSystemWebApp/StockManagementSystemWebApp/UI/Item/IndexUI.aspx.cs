﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StockManagementSystemWebApp.BLL.Manager;

namespace StockManagementSystemWebApp.UI.Item
{
    public partial class indexUI : System.Web.UI.Page
    {
         private ItemManager itemManager;

        public indexUI()
        {
            itemManager=new ItemManager();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            itemGridViewAll.DataSource = itemManager.GetItemViewDetails();
            itemGridViewAll.DataBind();
        }


        protected void itemGridViewAll_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
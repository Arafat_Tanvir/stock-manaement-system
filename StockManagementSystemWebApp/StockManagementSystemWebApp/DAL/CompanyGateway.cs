﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StockManagementSystemWebApp.BLL.Models;

namespace StockManagementSystemWebApp.DAL
{
    public class CompanyGateway:BaseGateway
    {
        public int Save(Company company)
        {

            string query = "INSERT INTO Company VALUES('" + company.Name + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public bool IsCompanyNameExists(string name)
        {
            string query = "SELECT * FROM Company WHERE Name='" + name + "' ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool IsCompanyName = Reader.HasRows;
            Connection.Close();
            return IsCompanyName;
        }

        //Show All Companies
        public List<Company> ShowAllCompanies()
        {
            List<Company> companies = new List<Company>();
            string query = "SELECT * FROM Company ORDER BY Id DESC;";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Company company = new Company();
                company.Id = Convert.ToInt32(Reader["Id"]);
                company.Name = Reader["Name"].ToString();
                companies.Add(company);
            }
            Connection.Close();
            return companies;

        }

        public Company GetCompanyById(int id)
        {
            string query = "SELECT * FROM Company WHERE Id=" + id + "ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Company company = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                company = new Company();
                company.Id = Convert.ToInt32(Reader["Id"]);
                company.Name = Reader["Name"].ToString();
            }
            Connection.Close();
            return company;

        }

        public int UpdateCompany(Company company)
        {
            string query = "UPDATE Company set Name='" + company.Name + "'WHERE Id=" + company.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect =Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
    }
}
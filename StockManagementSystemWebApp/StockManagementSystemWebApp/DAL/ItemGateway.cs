﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StockManagementSystemWebApp.BLL.Models;

namespace StockManagementSystemWebApp.DAL
{
    public class ItemGateway:BaseGateway
    {
        //All item name load
        public List<GetItemView> GetAllItemName(int id) 
        {
            List<GetItemView> itemViews = new List<GetItemView>();
            string queryItem = "SELECT * FROM GetItemView WHERE CompanyId=" + id;
            Command = new SqlCommand(queryItem, Connection);
            Connection.Open();
            Reader= Command.ExecuteReader();
            while (Reader.Read())
            {
                GetItemView getItemView = new GetItemView();
                getItemView.Id = Convert.ToInt32(Reader["Id"]);
                getItemView.Name = Reader["Name"].ToString();
                itemViews.Add(getItemView);
            }
            Connection.Close();

            return itemViews;

        }

        //get item details
        public GetItemView GetItemDetailsViewById(int id)
        {
            string query = "SELECT * FROM GetItemView WHERE Id=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            GetItemView getItemView = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                getItemView = new GetItemView();
                getItemView.Id = Convert.ToInt32(Reader["Id"]);
                getItemView.Reorder = Convert.ToInt32(Reader["Reorder"]);
                getItemView.Name = Reader["Name"].ToString();
            }
            Connection.Close();
            return getItemView;

        }
       

        //for item save
        public int Save(Item item)
        {
            string queryItem = "INSERT INTO Item VALUES("+item.CategoryId+","+item.CompanyId+",'" + item.Name + "',"+item.Reorder+")";
            Command = new SqlCommand(queryItem, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        //Item Is Already Exits(Checked)
        public bool IsItemNameExists(string name)
        {
            string query = "SELECT * FROM Item WHERE Name='" + name + "'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool IsItemName = Reader.HasRows;
            Connection.Close();
            return IsItemName;
        }

        //show item details in GridView
        public List<GetItemView> GetItemViewDetails()
        {
            List<GetItemView> getItemViews = new List<GetItemView>();
            string query = "SELECT * FROM GetItemView";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                GetItemView getItemView = new GetItemView();
                getItemView.Id = Convert.ToInt32(Reader["Id"]);
                getItemView.CategoryId = Convert.ToInt32(Reader["CategoryId"]);
                getItemView.CategoryName = Reader["CategoryName"].ToString();
                getItemView.CompanyId = Convert.ToInt32(Reader["CompanyId"]);
                getItemView.CompanyName = Reader["CompanyName"].ToString();
                getItemView.Name = Reader["Name"].ToString();
                getItemView.Reorder = Convert.ToInt32(Reader["Reorder"]);
                getItemViews.Add(getItemView);
            }
            Connection.Close();
            return getItemViews;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StockManagementSystemWebApp.BLL.Models;

namespace StockManagementSystemWebApp.DAL
{

    public class CategoryGateway:BaseGateway
    {
        
        public int Save(Category category)
        {

            string query = "INSERT INTO Category VALUES('" + category.Name + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public bool IsCategoryNameExists(string name)
        {
            string query = "SELECT * FROM Category WHERE Name='" + name + "' ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool IsCatgoryName = Reader.HasRows;
            Connection.Close();
            return IsCatgoryName;
        }

        //show All Categories
        public List<Category> ShowAllCategories()
        {
            List<Category> categories=new List<Category>();
            string query = "SELECT * FROM Category ORDER BY Id DESC;";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Category category = new Category();
                category.Id = Convert.ToInt32(Reader["Id"]);
                category.Name = Reader["Name"].ToString();
                categories.Add(category);
            }
            Connection.Close();
            return categories;
            
        }

        public Category GetCategoryById(int id)
        {
            string query = "SELECT * FROM Category WHERE Id="+id+" ORDER BY Id DESC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Category category = null;
            SqlDataReader readerById = Command.ExecuteReader();
            if (readerById.Read())
            {
                category = new Category();
                category.Id = Convert.ToInt32(readerById["Id"]);
                category.Name = readerById["Name"].ToString();
            }
            Connection.Close();
            return category ;

        }

        public int UpdateCategory(Category category)
        {
            string query = "UPDATE Category set Name='"+category.Name+"'WHERE Id="+category.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
    }
}
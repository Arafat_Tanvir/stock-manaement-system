﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StockManagementSystemWebApp.BLL.Models;

namespace StockManagementSystemWebApp.DAL
{
    public class StockInGateway:BaseGateway
    {
        //when click the companyid select then load all item name
        public List<GetStockInView> GetAllItemNameViewById(int id)
        {
            List<GetStockInView> getAllName = new List<GetStockInView>();
            string query = "SELECT * FROM GetStockInView WHERE CompanyId=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                GetStockInView getStockInView = new GetStockInView();
                getStockInView.Id = Convert.ToInt32(Reader["Id"]);
                getStockInView.ItemId = Convert.ToInt32(Reader["ItemId"]);
                getStockInView.ItemName = Reader["ItemName"].ToString();
                getAllName.Add(getStockInView);
            }
            Connection.Close();
            return getAllName;
        }

        //No Item StockIn Talbe so  New Save
        public int Save(StockIn stockIn)
        {
            string query = "INSERT INTO StockIn VALUES(" + stockIn.ItemId + ","+stockIn.StockInQuantity+" )";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        //Allready item In StockIn Table so just add sum then store
        public int QuanityUpdateByItemId(StockIn stockIn)   
        {
            string query = "UPDATE StockIn set StockInQuantity='" + stockIn.StockInQuantity + "'WHERE Id=" + stockIn.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        //How Many Quantity Available In for Selected Item
        public GetStockInView GetAvailabeQuatityViewById(int id)    
        {

            string query = "SELECT * FROM GetStockInView WHERE Itemid=" + id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            GetStockInView getStockInView = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                getStockInView = new GetStockInView();
                getStockInView.Id = Convert.ToInt32(Reader["Id"]);
                getStockInView.Reorder =Convert.ToInt32(Reader["Reorder"]);
                getStockInView.ItemId = Convert.ToInt32(Reader["ItemId"]);
                getStockInView.StockInQuantity = Convert.ToInt32(Reader["StockInQuantity"]);
            }
            Connection.Close();
            return getStockInView;
        }
        //Show All Impormation For Items
        public List<GetStockInView> GetStockInView()
        {
            List<GetStockInView> getStockInViews = new List<GetStockInView>();
            string query = "SELECT * FROM GetStockInView";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                GetStockInView getStockInView = new GetStockInView();
                getStockInView.Id = Convert.ToInt32(Reader["Id"]);
                getStockInView.CompanyName = Reader["CompanyName"].ToString();
                getStockInView.ItemName = Reader["ItemName"].ToString();
                getStockInView.Reorder = Convert.ToInt32(Reader["Reorder"]);
                getStockInView.StockInQuantity = Convert.ToInt32(Reader["StockInQuantity"]);
                getStockInViews.Add(getStockInView);
            }
            Connection.Close();
            return getStockInViews;

        }
        //get  Company Or category selected Show All Impormation
        public List<GetStockInView> SearchByCategoryOrCompany(GetStockInView stockInView)
        {
            List<GetStockInView> getStockInViews = new List<GetStockInView>();
            string query = "SELECT * FROM GetStockInView WHERE CategoryId=" + stockInView.CategoryId+ " OR CompanyId="+stockInView.CompanyId+"";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                GetStockInView getStockInView = new GetStockInView();
                getStockInView.Id = Convert.ToInt32(Reader["Id"]);
                getStockInView.CategoryId = Convert.ToInt32(Reader["CategoryId"]);
                getStockInView.CategoryName = Reader["CategoryName"].ToString();
                getStockInView.CompanyId = Convert.ToInt32(Reader["CompanyId"]);
                getStockInView.CompanyName = Reader["CompanyName"].ToString();
                getStockInView.ItemId = Convert.ToInt32(Reader["ItemId"]);
                getStockInView.ItemName = Reader["ItemName"].ToString();
                getStockInView.Reorder = Convert.ToInt32(Reader["Reorder"]);
                getStockInView.StockInQuantity = Convert.ToInt32(Reader["StockInQuantity"]);
                getStockInViews.Add(getStockInView);
            }
            Connection.Close();
            return getStockInViews;

        }
        //stockInQuantity Update When Sell the Item
        public int QuanitySellByItemId(StockIn stockIn)
        {
            string query = "UPDATE StockIn set StockInQuantity='" + stockIn.StockInQuantity + "'WHERE ItemId=" + stockIn.ItemId;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
    }
}
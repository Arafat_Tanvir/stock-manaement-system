﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using StockManagementSystemWebApp.BLL.Models;
using StockManagementSystemWebApp.BLL.Models.ModelView;

namespace StockManagementSystemWebApp.DAL
{
    public class StockOutGateway:BaseGateway
    {
        public GetStockOutView StockOutQuatityView(int id)
        {

            string query = "SELECT * FROM GetStockOutView WHERE Itemid=" + id + " AND Status=" + 0;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            GetStockOutView stockOutView = null;
            Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                stockOutView = new GetStockOutView();
                stockOutView.Id = Convert.ToInt32(Reader["Id"]);
                stockOutView.ItemId = Convert.ToInt32(Reader["ItemId"]);
                stockOutView.StockOutQuantity = Convert.ToInt32(Reader["StockOutQuantity"]);
                stockOutView.Date = (DateTime)Reader["Date"];
            }
            Connection.Close();
            return stockOutView;
        }

        //then item not sell 
        public int QuanityUpdateByItemId(StockOut stockOut)
        {
            string query = "UPDATE StockOut set StockOutQuantity='" + stockOut.StockOutQuantity + "'WHERE Id=" + stockOut.Id;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        public int Save(StockOut stockOut)
        {
            string query = "INSERT INTO StockOut VALUES(" + stockOut.ItemId + "," + stockOut.StockOutQuantity + ",'" + stockOut.Date + "','" + stockOut.Status + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public List<GetStockOutView> GetStockOutView()
        {
            List<GetStockOutView> getStockOutViews = new List<GetStockOutView>();
            string query = "SELECT * FROM GetStockOutView WHERE Status=0";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                GetStockOutView getStockOutView = new GetStockOutView();
                getStockOutView.Id = Convert.ToInt32(Reader["Id"]);
                getStockOutView.CompanyId = Convert.ToInt32(Reader["CompanyId"]);
                getStockOutView.ItemId = Convert.ToInt32(Reader["ItemId"]);
                getStockOutView.CompanyName = Reader["CompanyName"].ToString();
                getStockOutView.ItemName = Reader["ItemName"].ToString();
                getStockOutView.Status = Convert.ToInt32(Reader["Status"]);
                getStockOutView.StockOutQuantity = Convert.ToInt32(Reader["StockOutQuantity"]);
                getStockOutViews.Add(getStockOutView);
            }
            Connection.Close();
            return getStockOutViews;

        }
        //When Sell The Item Then Status=1(Update)
        public int SellItem(StockOut stockOut)
        {
            string query = "UPDATE StockOut set Status= "+stockOut.Status+" WHERE ItemId=" + stockOut.ItemId;
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }

        public List<GetStockOutView> GetSearbyDate(SearchByDate searchByDate)
        {
            List<GetStockOutView> getStockOutViews = new List<GetStockOutView>();
            string query = "SELECT * FROM GetStockOutView WHERE (Date BETWEEN '" + searchByDate.DateFrom + "' AND '" + searchByDate.DateTo + "') AND Status=1";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                GetStockOutView getStockOutView = new GetStockOutView();
                getStockOutView.Id = Convert.ToInt32(Reader["Id"]);
                getStockOutView.CompanyId = Convert.ToInt32(Reader["CompanyId"]);
                getStockOutView.ItemId = Convert.ToInt32(Reader["ItemId"]);
                getStockOutView.CompanyName = Reader["CompanyName"].ToString();
                getStockOutView.ItemName = Reader["ItemName"].ToString();
                getStockOutView.Status = Convert.ToInt32(Reader["Status"]);
                getStockOutView.StockOutQuantity = Convert.ToInt32(Reader["StockOutQuantity"]);
                getStockOutViews.Add(getStockOutView);
            }
            Connection.Close();
            return getStockOutViews;

        }
    }
}